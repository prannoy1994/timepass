//Makes an API call to text razor API,parses the json response and sends the relevant information to Arduino Mega

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class nlp {

	/**
	 * @param args
	 */
	public static JSONObject textRazor(String text, String extractors) {
		JSONObject result = new JSONObject();

		try {

			// The URL for the API operation that you are attempting to
			// complete.
			String postUrl = "http://api.textrazor.com/";

			// Create a HTTP Post Request.
			HttpPost postRequest = new HttpPost(postUrl);

			// Set the post parameters.
			StringEntity postParamsEntity = new StringEntity(
					"apiKey=fa153da89d19b7513a97a5a074a2819023c1edba0e50510116e06906&text="
							+ text + "&extractors=" + extractors);
			postParamsEntity
					.setContentType("application/x-www-form-urlencoded");
			// Add the post parameters to the post request.
			postRequest.setEntity(postParamsEntity);

			// Create a HTTP Client.
			DefaultHttpClient httpClient = new DefaultHttpClient();

			// Execute the HTTP POST
			System.out.println("Executing HTTP Post...\n");
			HttpResponse response = httpClient.execute(postRequest);

			// Check the HTTP status of the post.
			if (response.getStatusLine().getStatusCode() != 200
					&& response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed: HTTP error code: "
						+ response.getStatusLine().getStatusCode());
			}

			// Create a reader to read in the HTTP post results.
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			// Read in all of the post results into a String.
			String output = "";
			Boolean keepGoing = true;
			while (keepGoing) {
				String currentLine = br.readLine();

				if (currentLine == null)
					keepGoing = false;
				else
					output += currentLine;
			}

			// Convert the post results from a String to a JSONObject.
			result = (JSONObject) new JSONParser().parse(output);
			System.out.println(result);

			// Close the http client.
			httpClient.getConnectionManager().shutdown();

		} catch (MalformedURLException e) {
			System.out.println("Caught MalformedURLException: " + e.toString());

		} catch (IOException e) {
			System.out.println("Caught IOException: " + e.toString());

		} catch (Exception e) {
			System.out.println("Caught Exception: " + e.toString());
		}
		return result;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SerialTest main = new SerialTest();
		main.initialize();
		char location = 48;
		char state = 48;
		JSONObject response = new JSONObject();
		// JSONParser parser = new JSONParser();

		response = textRazor(
				"Jarvis Switch off all the lights in the living room",
				"entailments");
		// System.out.println(response);
		JSONObject details = (JSONObject) response.get("response");
		System.out.println(details);
		JSONArray entailments = (JSONArray) details.get("entailments");
		for (int i = 0; i < entailments.size(); i++) {
			JSONObject temp = (JSONObject) entailments.get(i);
			JSONArray word = (JSONArray) temp.get("entailedWords");
			// System.out.println(word.get(0));
			if ((word.get(0)).equals("on")) {
				int j = i;
				while (j < entailments.size()) {
					JSONObject temp1 = (JSONObject) entailments.get(j);
					JSONArray object = (JSONArray) temp1.get("entailedWords");
					// System.out.println(object.get(0));
					if ((object.get(0)).equals("light"))
					{
						System.out.println("Light!!!!");
					    state = 49;
					}
					if ((object.get(0)).equals("fan"))
						;// Notify Arduino to Switch on fan,location yet to
							// known
					j++;
				}
			}
			if ((word.get(0)).equals("in")) {
				int j = i;
				while (j < entailments.size()) {
					JSONObject temp1 = (JSONObject) entailments.get(j);
					JSONArray object = (JSONArray) temp1.get("entailedWords");
					if ((object.get(0)).equals("living"))
					{
						System.out.println("Living Room!!");
					    location = 49;
					}
					if ((object.get(0)).equals("bedroom"))
						;// Notify the location to arduino
					j++;
				}
			}
		}

		for (int i = 0; i < 50; i++)
		{
		main.writeData(location);
		//for (int i = 0; i < 50; i++)
		main.writeData(state);
		}
		System.out.println("Started");

	}

}
