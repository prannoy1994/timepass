import java.util.Properties;

 /*Script to parse an excel sheet and send out emails to all contacts in the file personalizing each mail with name etc */

import javax.mail.Message; 
import javax.mail.Session; 
import javax.mail.Transport; 
import javax.mail.internet.InternetAddress; 
import javax.mail.internet.MimeMessage; 
import javax.mail.*; 
import javax.mail.internet.*; 

import javax.activation.FileDataSource; 
import javax.activation.DataHandler; 
import java.io.File;

import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.common.LengthConverter;
import jxl.read.biff.BiffException;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class gmail 
{ 
private static final String hostName = "smtp.gmail.com"; 
private static final int hostPort = 465; 
private static final String senderAddress = "your email address"; 
private static final String password = "your password"; 
final static int n=65;//enter number of merchants in the list
final static int c=2;//number of columns for each merchant

public void sendEmail(String[] mess) throws Exception 
{ 
//Properties 
Properties props = new Properties(); 
props.put("mail.transport.protocol", "smtps"); 
props.put("mail.smtps.host", hostName); 
props.put("mail.smtps.auth", "true"); 

//Session 
Session mailSession = Session.getDefaultInstance(props); 
mailSession.setDebug(true); 

//Text 
MimeBodyPart text = new MimeBodyPart(); 
//text.setContent("<p>Hi "+mess[0]+",<br><br>Hope you're doing fine. It's really great to connect with you.<br><br>Okay, let me get the bare essentials out of the way first. <b> <a href=\"http://www.conquest.org.in\">Conquest</a> is <a href=\"http://www.bits-pilani.ac.in/Pilani/index.aspx\">BITS Pilani</a>'s Annual International Startup Conclave that's been sending ripples in the startup ecosystem since 2004</b>. We've been scaling up exponentially every year, but this year, because of a team that does not know the concept of practical inhibition, we have truly made our reach exceed our grasp. <br><br>This year we received applications from over a 1000 startups (a 2.5x increase), including the winning startups and ideas from some of the best competitions around (TiE 50, Holt Global Challenge, Startup Chile etc), with a lot of (read 20%) international participation as well. So, our judges and mentors are going to have a hard time choosing the best 10 from so many great ventures. With the quality of the startups we have right now, and the sound business acumen of the visiting VCs, <b>we expect every team to raise between $500K to $3M from our <a href=\"http://conquest.org.in/sponsors.php\">investment partners</a></b>. And that makes <b>Conquest the supreme commander of Startup Challenges in India</b>. <br><br>And now to the crux of the matter, which is where you would fit in Operation Conquest. Since I'm positive I did a decent job at convincing you that Conquest is worth being a part of, <b>we'd like to have "+mess[1]+" as a sponsor for Conquest 2013</b>. I am sure we can do great things together for "+mess[1]+" as well as for the vast majority of startups at Conquest. <br><br>Looking forward to hearing from you.<br><br>PS: Really sorry the insanely long mail, but it's so very difficult to fit Conquest into anything smaller!<br><br>Thanks and regards<br>Prannoy Pilligundla<br>Sponsorship Coordinator:<a href=\"http://www.conquest.org.in\">Conquest 2013</a><br>BITS Pilani<br>+91 8441000486</p>","text/html"); 
text.setContent("<p>Greetings from Conquest 2013, BITS Pilani!<br><br>With the help Conquest has received from its well wishers across the country, we have been able to make Conquest 2013 India's biggest start-up challenge and now it is imminent, the day we've all been waiting for, the day of the Conquest Grand Finale in Delhi. We are inviting college students to join a community of students passionate about Startups and Entrepreneurship. As an Ambassador, you&#8217;ll be kept aware of emerging startups and business all across the world and will get an opportunity to attend Conquest in New Delhi free of charge. So make sure to check out our Campus Ambassador page on our website, build your skills and ignite your career!<br><br> What's in it for you? Well, a LOT, actually :<br>- One free ticket for the Grand finale for you on sale of at least one ticket to your buddies.<br>- A 50%( Rs 375) discount on all the tickets you sell to your buddies on the original price of Rs 750 which means you get the ticket for just Rs 375. (Discount Code is \"CON13+CAMP+AM\")<br>- A Certificate which certifies you to be a off-campus volunteer for the Center for Entrepreneurial Leadership ( CEL ), BITS    Pilani.<br>- A chance to get one of the ten exclusive CORPORATE passes worth Rs 2500, if you are one of our top three sellers!<br><br>Before you ask us about the corporate pass let us explain it to you.<br>A normal ticket gives you access to the conclave including panel discussions by eminent personalities, presentations by India's top ten Start-ups and of course a free lunch where as in addition to the above a Corporate pass will get you exclusive access to the biggest investors and Venture Capitalists and the best among the best entrepreneurs in the country. If this isn't enough, you will also be present for the networking session which is meant only for the top ten start-ups and investors. Needless to mention, you will be seated in the best seats at our venue!<br><br>We need one favor from you for D-Day to be a ginormous hit - we would want you to publicize the Grand Finale of Conquest 2013 in your campus. We have some suggestions for you on how you can publicize:<br>- Put up Conquest posters in strategic locations in your college.<br>- Share our <a href=\"http://www.facebook.com/Conquest.CEL\">Facebook posts</a> on groups<br>- Get your friends to follow us on <a href=\"https://twitter.com/anishshah101\">Twitter</a><br>- Adding friends on our <a href=\"https://www.facebook.com/events/424960614280393/\">event page</a><br>- Share our <a href=\"http://em.explara.com/event/conquest-2013-finale\">ticketing portal</a> and the discount codes<br>We know you want to be a part of this start-up revolution, which is why we contacted you in the first place. Do keep in touch with us for further details. Conquest 2013. Innovate. Achieve. Conquer.<br><br>Thanks and regards<br>Srikanth Garimella<br>Operations Associate:<a href=\"http://www.conquest.org.in\"> Conquest 2013</a><br>BITS Pilani<br>+91 8742035425</p>","text/html");

//Attachment 
FileDataSource fds = new FileDataSource("F:\\Poster 13.jpg"); 
MimeBodyPart attachment = new MimeBodyPart(); 
attachment.setDataHandler(new DataHandler(fds)); 
attachment.setFileName(fds.getName()); 

//Create Multipart & Add Parts 
Multipart content = new MimeMultipart(); 
content.addBodyPart(text); 
content.addBodyPart(attachment); 

//Compile Message 
MimeMessage message = new MimeMessage(mailSession); 
message.setSubject("Conquest 2013(BITS-Pilani)"); 
message.setContent(content);
//message.addRecipient(Message.RecipientType.TO, new InternetAddress("garisrik@gmail.com")); 
message.addRecipient(Message.RecipientType.TO, new InternetAddress(mess[0].trim())); 

//Send Message 
Transport transport = mailSession.getTransport(); 
transport.connect(hostName, hostPort, senderAddress, password); 
transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO)); 
transport.close(); 
} 

public static void main(String[] args) throws Exception 
{ 
 

String[] data=new String[2];


try {
   
       //Create a workbook object from the file at specified location.
       //Change the path of the file as per the location on your computer.
       Workbook wrk1 =  Workbook.getWorkbook(new File("F:/Book1.xls"));
       
        
       //Obtain the reference to the first sheet in the workbook
       Sheet sheet1 = wrk1.getSheet(0);
       
       
       //Iterate over row 0 and get the column names
    
       for(int i=12;i<n;i++)
       {
       for(int j=0,h=0;j<c;j++)
       //Obtain reference to the Cell using getCell(int col, int row) method of sheet
          { 
    	   //if(j==1 || j==2 || j==3)
    	   //{
           	Cell colArow1 = sheet1.getCell(j,i);
            String str_colArow1 = colArow1.getContents();
            data[h]=str_colArow1;
            //System.out.println("Contents of cell Col"+j+" Row "+i+": \""+str_colArow1 + "\"");
            h++;
    	   //} 
          }	
       new gmail().sendEmail(data);
     
       }
       
       
       
}catch (BiffException e) {
    e.printStackTrace();
} catch (IOException e) {
    e.printStackTrace();
}
}
}

